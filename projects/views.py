import os
import pandas as pd

from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import F
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render
from django.urls.base import reverse_lazy
from django.utils.safestring import mark_safe
from django.views.generic.base import TemplateView, View
from django.views.generic.edit import UpdateView
from django.views.generic.list import ListView
from django.db import transaction, connection
from markdown import markdown

from projects.forms import ProjectForm
from projects.models import Project, ProjectHistory
from django.shortcuts import redirect


class AssignmentView(TemplateView):
    template_name = 'projects/assignment.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        with open(os.path.join(os.path.dirname(settings.BASE_DIR), 'projement/README.md'), encoding='utf-8') as f:
            assignment_content = f.read()

        context.update({
            'assignment_content': mark_safe(markdown(assignment_content))
        })

        return context


class DashboardView(LoginRequiredMixin, ListView):
    model = Project
    context_object_name = 'projects'
    template_name = 'projects/dashboard.html'

    def get_queryset(self):
        projects = super().get_queryset()
        projects = projects.select_related('company').order_by(F('end_date').desc(nulls_first=True))

        return projects


class DownloadView(View):

    def get(self, request, *args, **kwargs):
        projects = Project.objects.all()

        exported_data = []
        for project in projects:
            exported_data.append({
                'Title': project.title,
                'Company Name': project.company,
                'Total Actual Hours': project.total_actual_hours,
                'Total Estimated Hours': project.total_estimated_hours
            })

        df = pd.DataFrame(exported_data)
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = f'attachment;filename="Projects.csv"'
        df.to_csv(response, encoding='utf-8', header=True)
        return response


class ProjectUpdateView(LoginRequiredMixin, UpdateView):
    model = Project
    form_class = ProjectForm
    success_url = reverse_lazy('dashboard')

    def post(self, request, *args, **kwargs):
        updated_model = get_object_or_404(self.model, pk=kwargs.get('pk'))
        form = self.form_class(request.POST)

        if form.is_valid():
            cleaned_data = form.cleaned_data
            if cleaned_data.get('actual_design') == 0 and cleaned_data.get(
                    'actual_development') == 0 and cleaned_data.get('actual_development') == 0:
                return redirect(self.success_url)

            with transaction.atomic():
                ProjectHistory.objects.create(
                    user=request.user,
                    actual_design=cleaned_data.get('actual_design'),
                    actual_development=cleaned_data.get('actual_development'),
                    actual_testing=cleaned_data.get('actual_development'),
                    actual_design_old=updated_model.actual_design,
                    actual_development_old=updated_model.actual_development,
                    actual_testing_old=updated_model.actual_testing,
                )

                updated_model.actual_design += cleaned_data.get('actual_design')
                updated_model.actual_development += cleaned_data.get('actual_development')
                updated_model.actual_testing += cleaned_data.get('actual_development')
                updated_model.save()

            print(print(connection.queries), 'test axper')
        return redirect(self.success_url)

    def get(self, request, *args, **kwargs):
        project = get_object_or_404(self.model, pk=kwargs.get('pk'))
        form = self.form_class(instance=project)
        updated_form = self.form_class
        context = {'form': form, 'updated_form': updated_form}

        return render(request, 'projects/project_form.html', context)
