from django.contrib import admin
from projects.models import Company, Project, Tags


class TagsAdmin(admin.ModelAdmin):
    list_display = ('title', 'created_at')
    list_filter = ('title',)
    ordering = ('-created_at',)
    search_fields = ('title',)


class TagsInline(admin.TabularInline):
    model = Tags.project.through


class ProjectAdmin(admin.ModelAdmin):
    list_display = ('title', 'company', 'start_date', 'end_date')
    list_filter = ('company',)
    ordering = ('-start_date',)
    inlines = (TagsInline,)

    fieldsets = (
        (None, {'fields': ['company', 'title', 'start_date', 'end_date']}),
        ('Estimated hours', {'fields': ['estimated_design', 'estimated_development', 'estimated_testing']}),
        ('Actual hours', {'fields': ['actual_design', 'actual_development', 'actual_testing']}),
    )

    def get_readonly_fields(self, request, obj=None):
        if obj is None:
            return ()

        return 'company',


admin.site.register(Company)
admin.site.register(Project, ProjectAdmin)
admin.site.register(Tags, TagsAdmin)
